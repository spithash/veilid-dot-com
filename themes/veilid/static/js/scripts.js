$(document).ready(function(){

    // Copy code snippets to the clipboard
    $('.code-snippet').on('click',function(){
        navigator.clipboard.writeText($(this).children('input').val());
    });

});