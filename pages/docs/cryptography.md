---
title: Cryptography
description: An overview of the cryptography used in Veilid
weight: 23
layout: subpage
---


Strong, appropriate, cryptography choices are essential to the functioning of Veilid.

Veilid provides applications guarantees about how data is handled on the wire and at rest.

Cryptosystems were chosen that work well together and provide a balance of speed and cryptographic hardness.

### Current Cryptography Systems

<ul>
  <li>
    <h4>Authentication is Ed25519</h4>
    Elliptic curve25519 was chosen to provide public/private key authentication and signing capabilities
  </li>
  <li>
    <h4>Key Exchange is x25519</h4>
    Curve25519 has a DH function that allows nodes to generate a symmetric key to communicate privately.
  </li>
  <li>
    <h4>Encryption is XChaCha20-Poly1305</h4>
    ChaCha20 with a 192-bit extended nonce is a fast authenticated stream cipher with associated data (AEAD).
  </li>
  <li>
    <h4>Message Digest is BLAKE3</h4>
    BLAKE3 is a extremely fast cryptographic hash that is highly parallelizable and as strong as SHA3-256 and over 17 times faster.
  </li>
  <li>
    <h4>Key Derivation is Argon2</h4>
    Password hash generation should be slow and resistant to GPU attacks Argon2 was the winner of the 2015 Password Hashing Competition.
  </li>
</ul>

### Upgrading Cryptography Systems

Nothing lasts forever and cryptography is no exception. As computing power improves and cryptographic attacks evolve, weaknesses in cryptosystems are inevitable.

Veilid has ensured that upgrading to newer cryptosystems is streamlined and minimally invasive to app developers, and handled transparently at the node level.

<ul>
  <li>
    <h4>Multiple Routing Tables</h4>
    Because changing cryptosystems changes node ids, there will be different distance measurements between nodes, necessitating a separate routing table per cryptosystem. We support this today.  
  </li>
  <li>
    <h4>Typed Keys</h4>
    Cryptographic keys, signatures, and hashes are all tagged with their cryptosystem to ensure that we know exactly how they were generated and how they should be used and persisted.
  </li>
  <li>
    <h4>Migration Support</h4>
    Reading persisted data will automatically use the correct cryptosystem and will default to always writing it back using the newest/best cryptosystem. This allows for data to be easily migrated just by reading it and writing it back to storage. 
  </li>
  <li>
    <h4>Simultaneous Cryptosystems</h4>
    While transitioning cryptosystems, nodes can respond to other nodes using either the old system or the new one, or both.
  </li>
</ul>

### Secure Storage



- Device-level secret storage APIs are available for all platforms
- Encrypted table store APIs are exposed to applications to make safe data storage easy
- Device data keys can also be password protected
- Apps never need to write anything to disk unencrypted

<div class="row g-3 mx-2 card-set">
  <div class="col-12 col-md-6">
    <div class="card">
      <div class="card-header text-bg-light">
        <h4>ProtectedStore</h4>
      </div>
      <div class="card-body">
        <p>Device-level Secret Storage</p>
        <ul>
          <li>MacOS / iOS Keychain</li>
          <li>Android Keystore</li>
          <li>Windows Protected Storage</li>
          <li>Linux Secret Service</li>
        </ul>
        New Rust Crate: <code>keyring-manager</code>
      </div>
    </div>
  </div>
  <div class="col-12 col-md-6">
    <div class="card">
      <div class="card-header text-bg-light">
       <h4>TableStore</h4>
      </div>
      <div class="card-body">
        <p>Encrypted Key-Value Database</p>
        <ul>
          <li>SQLITE on Native</li>
          <li>IndexedDB in Browser</li>
          <li>Device Key can be protected from backup dumping attacks</li>
        </ul>
        New Rust Crate: <code>keyvaluedb</code>
      </div>
    </div>
  </div>
  <div class="col-12 col-md-6">
    <div class="card">
      <div class="card-header text-bg-light">
       <h4>RecordStore</h4>
      </div>
      <div class="card-body">
        <p>Distributed Hash Table Storage</p>
        <ul>
          <li>Encrypted + Authenticated</li>
          <li>Subkey support</li>
          <li>LRU distributed cache</li>
          <li>Per-key multi-writer schemas</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-12 col-md-6">
    <div class="card">
      <div class="card-header text-bg-light">
        <h4>BlockStore</h4>
      </div>
      <div class="card-body">
        <p>Content-addressable Data Distribution
        <ul>
          <li>Take What You Give model</li>
          <li>Connect and share cloud storage</li>
          <li>Bittorrent-like sharding</li>
        </ul>
        This feature is "coming soon."
      </div>
    </div>
  </div>
</div>

### On The Wire

<div class="focus-text">
  <p>Everything is end-to-end encrypted</p>
  <p>Data is encrypted at rest and on the wire</p>
  <p>Your data is protected even if you lose your device</p>
</div>

<ul>
  <li>
    <h4>All Protocols Same Encryption</h4>
    Each low-level protocol uses the same message and receipt encapsulation. No protocol is special and all protocols offer the same safety guarantees.
  </li>
  <li>
    <h4>Encrypted And Signed</h4>
    Messages between nodes are signed by the sender and encrypted for only the receiver. Messages can be relayed without decryption and authentication covers the entire contents including headers. 
  </li>
  <li>
    <h4>Everything Is Timestamped</h4>
    Envelopes include timestamps and unique nonces and reject old or replayed messages. 
  </li>
  <li>
    <h4>Node Information Is Signed</h4>
    When a node publishes routing table entries they are signed. No node can lie about another node's dial info, capabilities, availability, or replay old node info when newer info is available. 
  </li>
</ul>
