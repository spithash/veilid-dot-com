---
title: Contribute
description: Learn about all the ways you can help Veilid
menu:
  main:
    weight: 6
weight: 1
layout: subpage
---

### How You Can Help

<ul>
  <li>
    <h4>Coders And Hackers</h4>
    We can use more low-level programmers and protocol experts. Platform experts. 
    We want this system to work well for everyone and be a strong foundation for 
    general computing and application development.
  </li>
  <li>
    <h4>App Developers</h4>
    You can get started writing a Veilid app today! Got a game idea? Want to port 
    something from a centralized system to a decentralized one? Let’s make this happen!
  </li>
  <li>
    <h4>Usability Experts</h4>
    We want to make sure that the framework and apps built with it are accessible to everyone. 
    Everyone should be able to make use of Veilid without even realizing they’re doing it.
  </li>
  <li>
    <h4>Translators</h4>
    Veilid technology is for everyone. Help us reach more people by helping us make sure as 
    many people as possible can read the documentation and use the applications.
  </li>
  <li>
    <h4>Open Source and Governance</h4>
    Open source projects deserve to be managed in the open too. We’ve got an open RFC process for our design and an MPL-2.0 license that ensures that free and commercial entities can contribute safely and legally.
  </li>
  <li>
    <h4><span title="Donations, but spell with a dollar sign for the letter s">Donation$</span></h4>
    Cash works, too. Donations are tax-deductible. <a href="/give">Support Veilid</a>
  </li>
</ul>

### Get Started

Please read our <a href="/contribute/code-of-conduct/">Code of Conduct</a> and then join us on our discord.

<a href="/discord" class="btn btn-primary btn-lg">Veilid Discord</a>
