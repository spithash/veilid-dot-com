---
title: Donate
description: Help support Veilid by gifting a tax-deductible donation
menu:
  main:
    weight: 10
layout: index
---

<div class="row g-5">
  <div class="col-12 col-md-7">
    <p>
      Veilid Foundation was formed to develop, distribute, and maintain a privacy focused 
      communication platform and protocol for the purposes of defending human and civil rights.
    </p>
    <p>
      Supporting us in this mission helps ensure the Veilid framework gets the support and defense
      it needs to protect your privacy.
    </p>
    <p>
      You have a very real chance at private, secure, and accessible communication tools.
    </p>
    <p>
      You can escape the data economy and help others do the same. You don't have to be the product.
    </p>
  </div>
  <div class="col-12 col-md-5">
    <div class="focus-text">
      <p>
        Veilid Foundation Inc is a registered 501c3 non-profit organization; your donations are tax-deductible.
      </p>
    </div>
    <p>You can learn more at <a href="https://veilid.org">Veilid.org</a></p>
  </div>
</div>

<div class="d-flex flex-column mt-5">
  <p class="small-warning order-2">
    Donations are collected via Stripe; clicking this link will open a new window or tab.
  </p>
  <p class="order-1">
    <a target="_blank" href="/give" class="btn btn-lg btn-success text-white w-100">Support Veilid Today</a>
  </p>
</div>

