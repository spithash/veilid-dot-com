---
title: Press
description: Information for the press and media about Veilid
layout: subpage
weight: 4
---

### Press Kit

PDF press summary: <a href="/Veilid-Framework-Press-Kit.pdf">Veilid Framework Press Kit</a>, 20.6 kB

### Press Contact
Press and media inquiries should be sent to <a href="mailto:press@veilid.org">press@veilid.org</a>

### In The News

- “America’s Original Hacking Supergroup Creates a Free Framework to Improve App Security.” _Engadget_, [https://www.engadget.com/americas-original-hacking-supergroup-creates-a-free-framework-to-improve-app-security-190043865.html](https://www.engadget.com/americas-original-hacking-supergroup-creates-a-free-framework-to-improve-app-security-190043865.html). Accessed 11 Aug. 2023.
- _Cult of Dead Cow Hacktivists Design Encryption System for Mobile Apps - Play Crazy Game_. 2 Aug. 2023, [https://playcrazygame.com/blog/2023/08/02/cult-of-dead-cow-hacktivists-design-encryption-system-for-mobile-apps/](https://playcrazygame.com/blog/2023/08/02/cult-of-dead-cow-hacktivists-design-encryption-system-for-mobile-apps/).
- “Cult of the Dead Cow Launches Encryption Protocol to Save Your Privacy.” _Gizmodo_, 2 Aug. 2023, [https://gizmodo.com/cult-of-the-dead-cow-launches-veilid-encryption-project-1850699803](https://gizmodo.com/cult-of-the-dead-cow-launches-veilid-encryption-project-1850699803).
- “Cult of the Dead Cow Wants to Save Internet Privacy with a New Encryption Protocol.” _Yahoo News_, 2 Aug. 2023, [https://news.yahoo.com/cult-dead-cow-wants-save-203200064.html](https://news.yahoo.com/cult-dead-cow-wants-save-203200064.html).
- Goodwins, Rupert. _Last Rites for UK’s Ridiculous Online Safety Bill_. [https://www.theregister.com/2023/08/21/opinion\_column\_monday/](https://www.theregister.com/2023/08/21/opinion_column_monday/). Accessed 21 Aug. 2023.
- Hemant, Kumar. “Hacktivist Group CDc to Unveil Veilid Encryption for Privacy-First Apps.” _Candid.Technology_, 2 Aug. 2023, [https://candid.technology/veilid-encryption-cdc-cult-of-dead-cow/](https://candid.technology/veilid-encryption-cdc-cult-of-dead-cow/).
- Johnson, Donovan. “Cult of the Dead Cow Develops Coding Framework for Privacy-Focused Apps.” _Fagen Wasanni Technologies_, 2 Aug. 2023, [https://fagenwasanni.com/ai/cult-of-the-dead-cow-develops-coding-framework-for-privacy-focused-apps/106537/](https://fagenwasanni.com/ai/cult-of-the-dead-cow-develops-coding-framework-for-privacy-focused-apps/106537/).
- Long, Heinrich. “Prominent Hacktivists to Launch Secure Messaging Framework Veilid.” _RestorePrivacy_, 5 Aug. 2023, [https://restoreprivacy.com/prominent-hacktivists-to-launch-secure-messaging-framework-veilid/](https://restoreprivacy.com/prominent-hacktivists-to-launch-secure-messaging-framework-veilid/).
- Menn, Joseph. “Hacking Group Plans System to Encrypt Social Media and Other Apps.” _Washington Post_, 2 Aug. 2023. _www.washingtonpost.com_, [https://www.washingtonpost.com/technology/2023/08/02/encryption-dead-cow-cult-apps-def-con/](https://www.washingtonpost.com/technology/2023/08/02/encryption-dead-cow-cult-apps-def-con/).
- RHC, Redazione. “Cult of the dead Cow presenta il protocollo Veilid. Una nuova alternativa al ‘capitalismo della sorveglianza.’” _Red Hot Cyber_, 3 Aug. 2023, [https://www.redhotcyber.com/post/cult-of-the-dead-cow-presenta-il-protocollo-veilid-una-nuova-alternativa-al-capitalismo-della-sorveglianza/](https://www.redhotcyber.com/post/cult-of-the-dead-cow-presenta-il-protocollo-veilid-una-nuova-alternativa-al-capitalismo-della-sorveglianza/).
- _Veilid Unveiled by Cult of the Dead Cow - Security - ITnews_. [https://www.itnews.com.au/news/veilid-unveiled-by-cult-of-the-dead-cow-599475](https://www.itnews.com.au/news/veilid-unveiled-by-cult-of-the-dead-cow-599475). Accessed 22 Aug. 2023.